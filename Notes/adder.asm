;
; NAME:    add.asm
; AUTHOR:  Kevin Cole ("The Ubuntourist") <kevin.cole@novawebdevelopment.org>
; LASTMOD: 2022.10.02 (je) <jeff.elkner@novawebdevelopment.org> to make it
;          work with asm80
;
; DESCRIPTION:
;
;     This is the assembly source code that produces the same machine
;     op codes as shown by the first example (add two numbers) in the
;     Altair 8800 Operator's Manual.
;
;     Assemble this source file with the command:
;
;         asl80 add.asm
;
;     which will generate the 8080 machine language add.com as output.
;
;     Using the online simulator (https://www.s2js.com/altair/sim.html#),
;     first load add_data.bin, and then add.com before running it.
;

VAL1    EQU	20h	; First addend is loaded from address 20h 
VAL2	EQU	21h	; Second addend is loaded from address 21h 
SUM 	EQU	30h	; Store sum at address 30h 

START:	LDA	VAL1	; Load value (5) into Accumulator
	MOV	B,A	; Move value in Accumulator to Register B
	LDA	VAL2	; Load value (8) into Accumulator
	ADD	B	; Add value in Register B to value in Accumulator
	STA	SUM	; Store accumulator at SUM (30h)
	JMP	START	; Jump to start of code (infinite loop)

	END		; End
