
## My Notes
Succesfully changed the storage location for the numbers and results by changing 0100 0000 to 0010 0000 which is from 80 to 20
![Adder Code](./adder_pic.jpeg)
Currently working on sequental index larger smaller program but having trouble figuring it out
As of 9/28/2022 I still have no idea how to do the sequential, still working with Olli on it. Could use Twos compliment or Liam's method. 
# 9/28/22
Waiting for Jeff to upload instructions for correcting path of 8080 assembler in .vshr
# 9/29/22
Still waiting on instructions, researching for loops and assembly language tonight.
![Loop Example](loopexample.png)
1.  CMP compares register to accumulator and stores result in Carry Bit
2. JZ Jump If Zero is a conditional instruction where if the Zero Bit is 1(meaning a zero is present) it jumps to the specified location after the JZ instruction, otherwise it continues sequential operation
3. INX simply increments a register pair of bytes
#10/2/22
![Upper Case Loop](upperCase.png)
Trying to figure this out. Been working on it for a few days hope to finish in class so I can make largest to smallest in assembly.

# 10/11/22
Spent 2 hours unsuccesfully getting adder to work. Still have error carrying twice
#10/12/22
After working from 9pm to 12am, I have succesfully created my working 8 bit adder
