#!/usr/bin/env python3
"""
Alex Fuhrig's Altair 8800 Assembler

This script assembles binary files for the Altair 8800. See the README for
details
"""

import sys
from io import TextIOWrapper


class Instruction:
    """
    This is a boilerplate class for instructions.
    """

    SINGLE_REGISTER_DICTIONARY: "dict[str, int]" = {
        "b": 0b000,
        "c": 0b001,
        "d": 0b010,
        "e": 0b011,
        "h": 0b100,
        "l": 0b101,
        "m": 0b110,
        "a": 0b111,
    }

    def __init__(self):
        pass

    def generate_bytes(self, arguments: "list[str]") -> bytes:
        """ Boilerplate Method """
        return b""


class SimpleInstruction(Instruction):
    """
    This is a class for simple instructions that
    always have the same bytecode and might have arguments.
    """

    def __init__(self, _byte_code: bytes, _number_of_arguments: int = 0):
        self.byte_code = _byte_code
        self.number_of_arguments = _number_of_arguments

    def generate_bytes(self, arguments: "list[str]") -> bytes:
        return_bytes = self.byte_code
        for argument_number in range(0, self.number_of_arguments):
            argument: str = arguments[argument_number]
            return_bytes += bytes.fromhex(argument)
        return return_bytes


class ComplexInstruction(Instruction):
    """
    This is a class for complex instructions that don't
    always have the same bytecode and might have arguments.
    """

    def __init__(
        self,
        _front_bits: str,
        _end_bits: str = None,
        _number_of_arguments: int = 0,
    ):
        self.front_bits: str = _front_bits
        self.end_bits: str = _end_bits
        self.number_of_arguments: int = _number_of_arguments

    def generate_bytes(self, arguments: "list[str]") -> bytes:
        argument_bits: str = arguments[0]
        front_bits_length: int = len(self.front_bits)
        argument_bits_length: int = len(argument_bits)
        front_int: int = int(self.front_bits, 2) * pow(
            2, 8 - front_bits_length
        )
        argument_int: int = int(argument_bits, 2) * pow(
            2, 8 - front_bits_length - argument_bits_length
        )
        if self.end_bits is None:
            byte_code: bytes = (front_int + argument_int).to_bytes(1, "little")
        else:
            end_int: int = int(self.end_bits, 2)
            byte_code: bytes = (front_int + argument_int + end_int).to_bytes(
                1, "little"
            )
        return_bytes: bytes = byte_code
        for argument_number in range(1, self.number_of_arguments):
            argument: str = arguments[argument_number]
            return_bytes += bytes.fromhex(argument)
        return return_bytes


class MOVInstruction(Instruction):
    """
    This class is for the MOV instruction which has special behavior for its
    bytecodes
    """

    def __init__(self):
        pass

    def generate_bytes(self, arguments: "list[str]") -> bytes:
        destination_register = self.SINGLE_REGISTER_DICTIONARY.get(
            arguments[0]
        )
        source_register = self.SINGLE_REGISTER_DICTIONARY.get(arguments[1])
        byte_code = (
            0b01000000 + destination_register * 8 + source_register
        ).to_bytes(1, "little")
        return byte_code


class CustomByte(Instruction):
    """
    This class allows for special bytes meant to be used as data for a program.
    """

    def __init__(self):
        pass

    def generate_bytes(self, arguments: "list[str]") -> bytes:
        return_bytes: bytes = b""
        for data_byte in arguments:
            return_bytes += bytes.fromhex(data_byte)
        return return_bytes


class Assembler:
    """ The class for the actual assembler """

    INSTRUCTION_DICTIONARY: "dict[str, Instruction]" = {
        ###### Command Instructions
        ### Input/Output Instructions
        "IN": SimpleInstruction(b"\xdb", _number_of_arguments=1),
        "OUT": SimpleInstruction(b"\xd3", _number_of_arguments=1),
        ### Interrupt Instructions
        "EI": SimpleInstruction(b"\xfb"),
        "DI": SimpleInstruction(b"\xf3"),
        "HLT": SimpleInstruction(b"\x76"),
        "RST": ComplexInstruction("11", _end_bits="111"),
        ### Carry Bit Instructions
        "CMC": SimpleInstruction(b"\x3f"),
        "STC": SimpleInstruction(b"\x37"),
        ### No Operation Instruction
        "NOP": SimpleInstruction(b"\x00"),
        ###### Single Register Instructions
        "INR": ComplexInstruction("00", _end_bits="100"),
        "DCR": ComplexInstruction("00", _end_bits="101"),
        "CMA": SimpleInstruction(b"\x2f"),
        "DAA": SimpleInstruction(b"\x27"),
        ###### Register Pair Instructions
        "PUSH": ComplexInstruction("11", _end_bits="0101"),
        "POP": ComplexInstruction("11", _end_bits="0001"),
        "DAD": ComplexInstruction("00", _end_bits="1001"),
        "INX": ComplexInstruction("00", _end_bits="0011"),
        "DCX": ComplexInstruction("00", _end_bits="1011"),
        "XCHG": SimpleInstruction(b"\xeb"),
        "XTHL": SimpleInstruction(b"\xe3"),
        ###### Rotate Accumulator Instructions
        "RLC": SimpleInstruction(b"\x07"),
        "RRC": SimpleInstruction(b"\x0f"),
        "RAL": SimpleInstruction(b"\x17"),
        "RAR": SimpleInstruction(b"\x1f"),
        ###### Data Transfer Instructions
        ### Data Transfer Instructions
        "MOV": MOVInstruction(),
        "STAX": ComplexInstruction("000", _end_bits="0010"),
        "LDAX": ComplexInstruction("000", _end_bits="1010"),
        ### Register/Memory to Accumulator Transfers
        "ADD": ComplexInstruction("10000"),
        "ADC": ComplexInstruction("10001"),
        "SUB": ComplexInstruction("10010"),
        "SBB": ComplexInstruction("10011"),
        "ANA": ComplexInstruction("10100"),
        "XRA": ComplexInstruction("10101"),
        "ORA": ComplexInstruction("10110"),
        "CMP": ComplexInstruction("10111"),
        ### Direct Addressing Instructions
        "STA": SimpleInstruction(b"\x32", _number_of_arguments=2),
        "LDA": SimpleInstruction(b"\x3a", _number_of_arguments=2),
        "SHLD": SimpleInstruction(b"\x22", _number_of_arguments=2),
        "LHLD": SimpleInstruction(b"\x2a", _number_of_arguments=2),
        ###### Immediate Instructions
        "LXI": ComplexInstruction(
            "00", _end_bits="0001", _number_of_arguments=1
        ),
        "MVI": ComplexInstruction(
            "00", _end_bits="110", _number_of_arguments=2
        ),
        "ADI": SimpleInstruction(b"\xc6", _number_of_arguments=1),
        "ACI": SimpleInstruction(b"\xce", _number_of_arguments=1),
        "SUI": SimpleInstruction(b"\xd6", _number_of_arguments=1),
        "SBI": SimpleInstruction(b"\xde", _number_of_arguments=1),
        "ANI": SimpleInstruction(b"\xe6", _number_of_arguments=1),
        "XRI": SimpleInstruction(b"\xee", _number_of_arguments=1),
        "ORI": SimpleInstruction(b"\xf6", _number_of_arguments=1),
        "CPI": SimpleInstruction(b"\xfe", _number_of_arguments=1),
        ###### Branching Instructions
        ### Jump Instructions
        "PCHL": SimpleInstruction(b"\xe9"),
        "JMP": SimpleInstruction(b"\xc3", _number_of_arguments=2),
        "JC": SimpleInstruction(b"\xda", _number_of_arguments=2),
        "JNC": SimpleInstruction(b"\xd2", _number_of_arguments=2),
        "JZ": SimpleInstruction(b"\xca", _number_of_arguments=2),
        "JNZ": SimpleInstruction(b"\xc2", _number_of_arguments=2),
        "JM": SimpleInstruction(b"\xfa", _number_of_arguments=2),
        "JP": SimpleInstruction(b"\xf2", _number_of_arguments=2),
        "JPE": SimpleInstruction(b"\xea", _number_of_arguments=2),
        "JPO": SimpleInstruction(b"\xe2", _number_of_arguments=2),
        ### Call Instructions
        "CALL": SimpleInstruction(b"\xcd", _number_of_arguments=2),
        "CC": SimpleInstruction(b"\xdc", _number_of_arguments=2),
        "CNC": SimpleInstruction(b"\xd4", _number_of_arguments=2),
        "CZ": SimpleInstruction(b"\xcc", _number_of_arguments=2),
        "CNZ": SimpleInstruction(b"\xc4", _number_of_arguments=2),
        "CM": SimpleInstruction(b"\xfc", _number_of_arguments=2),
        "CP": SimpleInstruction(b"\xf4", _number_of_arguments=2),
        "CPE": SimpleInstruction(b"\xec", _number_of_arguments=2),
        "CPO": SimpleInstruction(b"\xe4", _number_of_arguments=2),
        ### Return Instructions
        "RET": SimpleInstruction(b"\xc9"),
        "RC": SimpleInstruction(b"\xd8"),
        "RNC": SimpleInstruction(b"\xd0"),
        "RZ": SimpleInstruction(b"\xc8"),
        "RNZ": SimpleInstruction(b"\xc0"),
        "RM": SimpleInstruction(b"\xf8"),
        "RP": SimpleInstruction(b"\xf0"),
        "RPE": SimpleInstruction(b"\xe8"),
        "RPO": SimpleInstruction(b"\xe0"),
        ###### Custom Byte
        ### This is not an Altair instruction. It is for bytes to be placed
        ### at the end of a program that contain needed data.
        "dat": CustomByte(),
    }

    def __init__(
        self,
        _input_file_location: str,
        _debugs_enabled: bool,
        _output_file_location: str = None,
    ):
        self.file: TextIOWrapper = open(_input_file_location, encoding="utf-8")
        self.debugs_enabled = _debugs_enabled
        if _output_file_location is None:
            self.output_file_location: str = f"{_input_file_location[0:_input_file_location.rfind('.')]}.bin"
        else:
            self.output_file_location: str = _output_file_location
        self.assembled_bytes = b""

    def assemble(self):
        """ This method assembles the program found in the file variable into a bytes object """
        line_number: int = 0
        for line in self.file:
            line_number += 1
            line = line.replace("\n", "")
            if self.debugs_enabled is True:
                print(line)
            hash_index: int = line.find("#")
            if hash_index == 0:
                continue
            if hash_index != -1:
                line = line[0:hash_index]
            line_split: "list[str]" = line.split(",")
            instruction_mnemonic: str = line_split[0]
            arguments = line_split[1:]
            instruction: Instruction = self.INSTRUCTION_DICTIONARY.get(
                instruction_mnemonic
            )
            try:
                self.assembled_bytes += instruction.generate_bytes(arguments)
            except AttributeError:
                print(
                    f"unknown instruction on line {line_number}\nplease change the instruction and try again\nSTOPPING ASSEMBLER"
                )
                sys.exit()
            except ValueError:
                print(
                    f"invalid argument on line {line_number}\nplease change the argument and try again\nSTOPPING ASSEMBLER"
                )
                sys.exit()
            except IndexError:
                print(
                    f"missing one or more arguments on line {line_number}\nplease change the arguments and try again\nSTOPPING ASSEMBLER"
                )
                sys.exit()
            if self.debugs_enabled is True:
                print(self.assembled_bytes)

    def write(self):
        """ This method writes to file location defined in the output_file_location variable """
        with open(self.output_file_location, "wb") as output_file:
            output_file.write(self.assembled_bytes)
        print(f"wrote assembled program to {self.output_file_location}")
        print("ASSEMBLER FINISHED SUCCESSFULLY")


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("an input file location was not passed as an argument")
        print("STOPPING ASSEMBLER")
        sys.exit()
    elif len(sys.argv) == 2:
        assembler = Assembler(sys.argv[1], False)
        assembler.assemble()
        assembler.write()
    elif len(sys.argv) == 3:
        assembler = Assembler(
            sys.argv[1], False, _output_file_location=sys.argv[2]
        )
        assembler.assemble()
        assembler.write()
    elif len(sys.argv) >= 4:
        if sys.argv[3] == "print-debugs":
            assembler = Assembler(
                sys.argv[1], True, _output_file_location=sys.argv[2]
            )
        else:
            assembler = Assembler(
                sys.argv[1], True, _output_file_location=sys.argv[2]
            )
        assembler.assemble()
        assembler.write()
    if len(sys.argv) > 4:
        print("extra arguments were provided and ignored")
